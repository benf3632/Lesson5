#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(type, name), _width(width), _length(length)
{
	_points.push_back(a);
	Point temp(a.getX() + width, a.getY() + length);
	_points.push_back(temp);
}

myShapes::Rectangle::~Rectangle()
{
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


double myShapes::Rectangle::getArea() const
{
	return _width * _length;
}

double myShapes::Rectangle::getPerimeter() const
{
	return 2 * _width + 2 * _length;
}

void myShapes::Rectangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
}