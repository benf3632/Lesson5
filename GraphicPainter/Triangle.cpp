#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

Triangle::~Triangle()
{
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

double Triangle::getArea() const
{
	double dis1 = _points[0].distance(_points[1]);
	double dis2 = _points[1].distance(_points[2]);
	double dis3 = _points[2].distance(_points[0]);
	double s = (dis1 + dis2 + dis3) / 2;

	return sqrt(s* (s - dis1) * (s - dis2) * (s - dis3));
}

double Triangle::getPerimeter() const
{
	double dis1 = _points[0].distance(_points[1]);
	double dis2 = _points[1].distance(_points[2]);
	double dis3 = _points[2].distance(_points[0]);
	
	return dis1 + dis2 + dis3;
}

void Triangle::move(const Point& other)
{
	this->_points[0] += other;
	this->_points[1] += other;
	this->_points[2] += other;
}