#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;

	if (!_shapes.size())
	{
		deleteAllShapes();
	}

}

void Menu::menu()
{
	int sel, selShape, selMode;
	bool stop = false;
	while (!stop)
	{
		system("cls");
		printMenu();
		cin >> sel;
		getchar();
		switch (sel)
		{
		//add new shape		
		case ADD:
			system("cls"); //clears cmd screen
			printShapeOptions();
			cin >> selShape;
			getchar();

			//what shape to create
			switch (selShape)
			{
			case CIRCLE:
				createCircle();
				break;
			case ARROW:
				createArrow();
				break;
			case TRIANGLE:
				createTriangle();
				break;
			case RECTANGLE:
				createRectangle();
				break;
			}
			break;
		//move shape, print details or delete shape 
		case CHANGE:
			system("cls"); //clears cmd screen
			printShapes();
			if (_shapes.size())
			{
				cin >> selShape;
				getchar();
				
				printModes();

				cin >> selMode;
				getchar();

				//print move or delete
				switch (selMode)
				{
				case MOVE:
					moveShape(selShape);
					break;
				case DETAILS:
					printDetails(selShape);
					system("pause");
					break;
				case REMOVE:
					deleteShape(selShape);
					break;
				}
			}
			else
			{
				cout << "No shapes to modify or show details. Please add shapes" << endl;
				system("pause");
			}
			break;
		//deletes all shapes
		case DEL:
			deleteAllShapes();
			break;
		//exist the menu
		case EXIT:
			stop = true;
			break;
		}
	}


}

/*
Function that prints the main menu
*/
void Menu::printMenu()
{
	cout << "Enter 0 to add a new shape" << endl;
	cout << "Enter 1 to modify or get information from a current shape." << endl;
	cout << "Enter 2 to delete all of the shapes." << endl;
	cout << "Enter 3 to exit." << endl;
}

/*
Function that prints shapes that can be created
*/
void Menu::printShapeOptions()
{
	cout << "Enter 0 to add a circle." << endl;
	cout << "Enter 1 to add an arrow." << endl;
	cout << "Enter 2 to add a triangle." << endl;
	cout << "Enter 3 to add a rectangle." << endl;
}

/*
Function that draws all the shapes to the screen
*/
void Menu::drawAllShapes()
{
	for (unsigned int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

/*
Function that creates circle shape
*/
void Menu::createCircle()
{
	Circle* circle;
	double x, y;
	double radius;
	string name;

	cout << "Please enter X:" << endl;
	cin >> x;
	getchar();
	cout << "Please enter Y:" << endl;
	cin >> y;
	getchar();

	cout << "Please enter radius:" << endl;
	cin >> radius;
	getchar();

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;
	getchar();

	Point center(x, y);
	circle = new Circle(center, radius, "Circle", name);
	_shapes.push_back(circle);
	drawAllShapes();
}

/*
Function that creates arrow shape
*/
void Menu::createArrow()
{
	string name;
	double points[2][2];

	for (int i = 0; i < 2; i++)
	{
		cout << "Enter the X of point number: " << i + 1  << endl;
		cin >> points[i][0];
		getchar();

		cout << "Enter the Y of point number: " << i + 1 << endl;
		cin >> points[i][1];
		getchar();
	}

	cout << "Enter the name of the shape:" << endl;
	cin >> name;
	getchar();

	Point p1(points[0][0], points[0][1]);
	Point p2(points[1][0], points[1][1]);

	Arrow* arr = new Arrow(p1, p2, "Arrow", name);

	_shapes.push_back(arr);
	drawAllShapes();
}

/*
Function that creates triangle shape
*/
void Menu::createTriangle()
{
	string name;
	double points[3][2];

	for (int i = 0; i < 3; i++)
	{	
		cout << "Enter the X of point: " << i + 1 << endl;
		cin >> points[i][0];
		getchar();

		cout << "Enter the Y of point: " << i + 1 << endl;
		cin >> points[i][1];
		getchar();
	}

	cout << "Enter the name of the shape:" << endl;
	cin >> name;
	getchar();

	Point p1(points[0][0], points[0][1]);
	Point p2(points[1][0], points[1][1]);
	Point p3(points[2][0], points[2][1]);
	
	if (!((p1.getX() == p2.getX() && p1.getX() == p3.getX()) ||  (p1.getY() == p2.getY() && p1.getY() == p3.getY())))
	{
		Triangle* tri = new Triangle(p1, p2, p3, "Triangle", name);
		_shapes.push_back(tri);
		drawAllShapes();
	}
	else
	{
		cout << "The points create a line" << endl;
		system("pause");
	}
}

/*
Function that clears draw all the shapes from the screen
*/
void Menu::clearDrawAllShapes()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->clearDraw(*_disp, *_board);
	}
}

/*
Function that creates rectangle shape
*/
void Menu::createRectangle()
{
	double width, length;
	double x, y;

	string name;

	cout << "Enter X for the left corner:" << endl;
	cin >> x;
	getchar();

	cout << "Enter Y for the left corner:" << endl;
	cin >> y;
	getchar();

	cout << "Enter the length of the shape:" << endl;
	cin >> length;
	getchar();

	cout << "Enter the width of the shape:" << endl;
	cin >> width;
	getchar();

	cout << "Enter the name of the shape:" << endl;
	cin >> name;
	getchar();

	if (width <= 0 || length <= 0)
	{
		cout << "Cant create rectangle(length or width under 0)" << endl;
		system("pause");
	}
	else
	{

		Point p1(x, y);
		myShapes::Rectangle* rect = new myShapes::Rectangle(p1, width, length, "Rectangle", name);
		_shapes.push_back(rect);
		drawAllShapes();
	}
}

/*
Function that prints avialable shapes
*/
void Menu::printShapes() const
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		cout << "Enter " << i << " for ";
		_shapes[i]->printDetails();
		cout << endl;
	}
}

/*
Function that prints shape options
*/
void Menu::printModes() const
{
	cout << "Enter 0 to move the shape." << endl;
	cout << "Enter 1 to get its details." << endl;
	cout << "Enter 2 to remove the shape." << endl;
}

/*
Function that moves a shape
*/
void Menu::moveShape(int shapePos)
{
	double x, y;
	cout << "Please enter the X moving scale:" << endl;
	cin >> x;
	getchar();

	cout << "Please enter the Y moving scale:" << endl;
	cin >> y;
	getchar();

	Point p(x, y);

	clearDrawAllShapes();
	_shapes[shapePos]->move(p);
	drawAllShapes();
}

/*
Function that prints shape details(type, name, area, perimeter)
*/
void Menu::printDetails(int shapePos) const
{
	cout << _shapes[shapePos]->getType() << "\t" << _shapes[shapePos]->getName() << "\t" << _shapes[shapePos]->getArea() << "   " << _shapes[shapePos]->getPerimeter() << endl;
}

/*
Function that deletes a shape
*/
void Menu::deleteShape(int shapePos)
{
	Shape* shape = _shapes[shapePos];
	clearDrawAllShapes();
	_shapes.erase(_shapes.begin() + shapePos);
	delete shape;
	drawAllShapes();
}

/*
Function that deletes all the shapes
*/
void Menu::deleteAllShapes()
{
	clearDrawAllShapes();
	for (int i = 0; i < _shapes.size(); i++)
	{
		delete _shapes[i];
	}

	_shapes.clear();
}