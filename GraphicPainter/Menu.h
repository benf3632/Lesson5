#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


enum shape{CIRCLE, ARROW, TRIANGLE, RECTANGLE};
enum sel{ADD, CHANGE, DEL, EXIT};
enum mode{MOVE, DETAILS, REMOVE};


class Menu
{
public:

	Menu();
	~Menu();

	void menu();
	
	


private: 

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;

	vector<Shape*> _shapes;

	void drawAllShapes();
	void printMenu();
	void printShapeOptions();
	void clearDrawAllShapes();
	void printShapes() const;
	void printModes() const;

	//creates shapes
	void createCircle();
	void createArrow();
	void createTriangle();
	void createRectangle();

	//move, delete, details
	void moveShape(int shapePos);
	void printDetails(int shapePos) const;
	void deleteShape(int shapePos);

	void deleteAllShapes();
	
	

};

