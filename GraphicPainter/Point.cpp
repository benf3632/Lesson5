#include "Point.h"

Point::Point(double x, double y) : _x(x), _y(y)
{
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::~Point()
{
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow((_x - other._x), 2) + pow((_y - other._y), 2));
}

Point Point::operator+(const Point& other) const
{
	Point temp(other);
	temp._x += _x;
	temp._y += _y;
	return temp;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}
