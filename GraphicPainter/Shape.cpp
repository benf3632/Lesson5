#include "Shape.h"

Shape::Shape(const string & name, const string & type) : _name(name), _type(type)
{
}



void Shape::printDetails() const
{
	cout << _name << "(" + _type + ")";
}

string Shape::getType() const
{
	return _type;
}

string Shape::getName() const
{
	return _name;
}
